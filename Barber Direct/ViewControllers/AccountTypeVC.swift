//
//  AccountTypeVC.swift
//  Barber Direct
//
//  Created by Artyom Schiopu on 12/6/17.
//  Copyright © 2017 ideactionio. All rights reserved.
//

import UIKit

class AccountTypeVC: UIViewController, UITableViewDataSource, UITableViewDelegate {
  


 
    @IBOutlet weak var mainView: UIView!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.transform = CGAffineTransform(scaleX: Constants.scale, y: Constants.scale)

    }
   
    @IBAction func openMenuAction(_ sender: UIButton) {
        sideMenuVC.toggleMenu()
    }
  
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! AccountTypeCell
        cell.checkMarkView.image = UIImage(named: "done icon")
        cell.doneImage.image = UIImage(named: "done")
        cell.checkMarkView.isHidden = false
        cell.doneImage.isHidden = false
    }
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! AccountTypeCell
        cell.checkMarkView.isHidden = true
        cell.doneImage.isHidden = true
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AccountTypeCell", for: indexPath) as! AccountTypeCell
        if indexPath.row == 1{
            
             cell.typeOfAccount.text = "Freelancer account"
            cell.typeOfAccount.sizeToFit()
        }
       
        
        return cell
    }

 
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }

    
}
