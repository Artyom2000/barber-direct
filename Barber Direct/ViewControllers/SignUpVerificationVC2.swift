//
//  SignUpVerificationVC2.swift
//  Barber Direct
//
//  Created by ABC on 11/29/17.
//  Copyright © 2017 ideactionio. All rights reserved.
//

import UIKit

class SignUpVerificationVC2: UIViewController, UINavigationControllerDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.transform = CGAffineTransform(scaleX: Constants.scale, y: Constants.scale)

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func okayActionButton(_ sender: Any) {
        let url = URL(string: "http://barberdirect.project-demo.info/v1/Verifyphonenumber")!
        
        var concatenatedData:Data = Data();
        let parametre:[String:Any] = ["Phonenumber" : verifyCodeUser.phoneNumberUser,
                                      "Verificationcode": verifyCodeUser.unicCode,
                                      "Userid": UserInfo.userID]
        let datas = try! JSONSerialization.data(withJSONObject: parametre, options: []);
        concatenatedData.append(datas);
        
        let session = URLSession.shared
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = concatenatedData;
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            
            guard error == nil else {
                return
            }
            
            guard let data = data else {
                return
            }
            
            do {
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] {
                    print(json)
                    DispatchQueue.main.async {

                    let destination = self.storyboard?.instantiateViewController(withIdentifier: "accountTypeVC") as! AccountTypeVC
                    self.navigationController?.pushViewController(destination, animated: true)
                                    }
                    }
                
            } catch let error {
                print(error.localizedDescription)
            }
        })
        task.resume()
        
        
        
        
        
    }
    

}
