//
//  LottieTransition.swift
//  Barber Direct
//
//  Created by Artyom Schiopu on 12/27/17.
//  Copyright © 2017 ideactionio. All rights reserved.
//

import UIKit
import Lottie
class LottieTransition: UIViewController, UINavigationControllerDelegate,
UIViewControllerTransitioningDelegate{

    

    override func viewDidLoad() {
        super.viewDidLoad()

   
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
     
        let sb = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: "navigationController")
        vc.transitioningDelegate = self
      present(vc, animated: true, completion: nil)

        
    }
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning?{
        let animationController = LOTAnimationTransitionController(animationNamed: "vcTransition1", fromLayerNamed: "outLayer", toLayerNamed: "inLayer", applyAnimationTransform: false)
        return animationController
    }
}
