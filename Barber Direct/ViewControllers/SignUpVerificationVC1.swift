//
//  SignUpVerificationVC1.swift
//  Barber Direct
//
//  Created by ABC on 11/29/17.
//  Copyright © 2017 ideactionio. All rights reserved.
//

import UIKit

class SignUpVerificationVC1: UIViewController, UINavigationControllerDelegate, UITextFieldDelegate {


  
    @IBOutlet var verificationCode: UITextField!
    
    var timers = Timer()
    
    
    @IBOutlet var phoneNumberLabel: UILabel!
    var phoneNumber = String()

    override func viewDidLoad() {
        super.viewDidLoad()
   ShowAlert.sharedInstance.alertController(messageAlert: verifyCodeUser.unicCode, title: "Your verification code", vc: self)
        verificationCode.delegate = self
        if verifyCodeUser.phoneNumberUser != ""
        {
            self.phoneNumberLabel.text = verifyCodeUser.phoneNumberUser
         }
        customize()
        self.view.transform = CGAffineTransform(scaleX: Constants.scale, y: Constants.scale)


    }
    
    

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
     
        if let numberOfCharcter = verificationCode.text?.count {
            if numberOfCharcter >= 6 && string != "" {
                return false
            }
            
        }
   
            return true
    
   
    }

    @IBAction func verifyNumberActionBtn(_ sender: Any) {
        print(verifyCodeUser.unicCode)
//        if self.verificationCode.text! == verifyCodeUser.unicCode {
//            let destination = self.storyboard?.instantiateViewController(withIdentifier: "signUpVerification2") as! SignUpVerificationVC2
//            self.navigationController?.pushViewController(destination, animated: true)
//        } else {
//            alertController(messageAlert: "Your verification code isn't correct", title: "Error")
//        }
        let destination = self.storyboard?.instantiateViewController(withIdentifier: "signUpVerification2") as! SignUpVerificationVC2
        self.navigationController?.pushViewController(destination, animated: true)
    }
    func generateRandomDigits(_ digitNumber: Int) -> String {
        var number = ""
        for i in 0..<digitNumber {
            var randomNumber = arc4random_uniform(10)
            while randomNumber == 0 && i == 0 {
                randomNumber = arc4random_uniform(10)
            }
            number += "\(randomNumber)"
        }
        return number
    }
    @IBAction func resendAction(_ sender: Any) {
        
        
        if timers.isValid {
         
            alertController(messageAlert: "A new code allowed only once in 30 seconds", title: "Error")
        }
        else {
             timers = Timer.scheduledTimer(withTimeInterval: 30, repeats: false) { (timer) in}
                    let twilioSID = "twilioSID"
                    let twilioSecret = "twilioSecret"
                    let fromNumber = "%fromNumber"
                    //+18305417068
                   // let toNumber = "%2B373068514268"
                    let toNumber = verifyCodeUser.phoneNumberResend
                    let code = (Int(generateRandomDigits(6)))
                    let message = String(code!)
            
                    // Build the request
                    let request = NSMutableURLRequest(url: URL(string:"https://\(twilioSID):\(twilioSecret)@api.twilio.com/2010-04-01/Accounts/\(twilioSID)/SMS/Messages")!)
                    request.httpMethod = "POST"
                   // request.setValue("", forHTTPHeaderField: "")
                    request.httpBody = "From=\(fromNumber)&To=\(toNumber)&Body=\(message)".data(using: .utf8)
            
                    // Build the completion block and send the request
                    URLSession.shared.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) in
                        print("Finished")
                        if let data = data, let responseDetails = NSString(data: data, encoding: String.Encoding.utf8.rawValue) {
                            // Success
                            verifyCodeUser.unicCode = String(code!)
                            print("Response: \(responseDetails)")
                            self.alertController(messageAlert: "A new code has been send", title: "Warning")
                        } else {
                            self.alertController(messageAlert: error as! String, title: "Error")
                            print("Error: \(error)")
                        }
                    }).resume()
           
            
           
        }
        
    }
    func alertController(messageAlert: String, title: String){
        let alert = UIAlertController(title: "Error", message: messageAlert, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
        
    }

    
    func textFieldDidChange () {
        let attributedString = NSMutableAttributedString(string: verificationCode.text!)
        if verificationCode.text?.count != 0 {
            attributedString.addAttributes([NSKernAttributeName : 31], range: NSMakeRange(0, (verificationCode.text?.count)!-1))
            
        }
        
        let font = UIFont(name: "Avenir", size: 20)!
        attributedString.addAttribute(NSFontAttributeName, value: font, range: NSMakeRange(0, (verificationCode.text?.count)!))
        verificationCode.attributedText = attributedString
        verificationCode.font = UIFont.monospacedDigitSystemFont(ofSize: 20, weight:UIFontWeightRegular )
    }
  override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

    }
    func customize() {
     
 
        verificationCode.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged
        )
        let layer = UIView(frame: CGRect(x: 52, y: 246, width: 20, height: 1))
        layer.layer.borderWidth = 1
        layer.layer.borderColor = UIColor(red:0.04, green:0.83, blue:0.82, alpha:1).cgColor
        self.view.addSubview(layer)
        
        let layer1 = UIView(frame: CGRect(x: 92, y: 246, width: 20, height: 1))
        layer1.layer.borderWidth = 1
        layer1.layer.borderColor = UIColor(red:0.04, green:0.83, blue:0.82, alpha:1).cgColor
        self.view.addSubview(layer1)
        
        let layer2 = UIView(frame: CGRect(x: 137, y: 246, width: 20, height: 1))
        layer2.layer.borderWidth = 1
        layer2.layer.borderColor = UIColor(red:0.04, green:0.83, blue:0.82, alpha:1).cgColor
        self.view.addSubview(layer2)
        
        let layer3 = UIView(frame: CGRect(x: 180, y: 246, width: 20, height: 1))
        layer3.layer.borderWidth = 1
        layer3.layer.borderColor = UIColor(red:0.04, green:0.83, blue:0.82, alpha:1).cgColor
        self.view.addSubview(layer3)
        
        let layer4 = UIView(frame: CGRect(x: 220, y: 246, width: 20, height: 1))
        layer4.layer.borderWidth = 1
        layer4.layer.borderColor = UIColor(red:0.04, green:0.83, blue:0.82, alpha:1).cgColor
        self.view.addSubview(layer4)
       
        let layer5 = UIView(frame: CGRect(x: 267, y: 246, width: 20, height: 1))
        layer5.layer.borderWidth = 1
        layer5.layer.borderColor = UIColor(red:0.04, green:0.83, blue:0.82, alpha:1).cgColor
        self.view.addSubview(layer5)
   
        
        
    }

    
    @IBAction func changeMobileNumberAction(_ sender: Any) {
        let destination = self.storyboard?.instantiateViewController(withIdentifier: "updatePhoneNumberVC") as! UpdatePhoneNumberVC
        self.navigationController?.pushViewController(destination, animated: true)
        
        
    }
    
   


    
}

