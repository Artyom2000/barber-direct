//
//  Country.swift
//  Barber Direct
//
//  Created by Artyom Schiopu on 12/7/17.
//  Copyright © 2017 ideactionio. All rights reserved.
//

import UIKit

class Country: NSObject {
    let name: String
    let dial_code : String

    
    init(name: String, dial_code: String) {
        self.name = name
        self.dial_code = dial_code

    }
}
