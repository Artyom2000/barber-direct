//
//  UpdatePhoneNumberVC.swift
//  Barber Direct
//
//  Created by ABC on 12/4/17.
//  Copyright © 2017 ideactionio. All rights reserved.
//

import UIKit

class UpdatePhoneNumberVC: UIViewController, UINavigationControllerDelegate {

    @IBOutlet weak var issueView: UIView!
    @IBOutlet weak var backgroundView: UIView!
    
    @IBOutlet weak var numberPhoneUser: UITextField!
    @IBOutlet weak var codeCountryLabel: UILabel!
    @IBOutlet weak var countryLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
//        issueView.isHidden = true
//        backgroundView.isHidden = true
        
        
        if shareCountryIndex.nameCountry != "" {
            self.countryLabel.text = shareCountryIndex.nameCountry
        }
        if shareCountryIndex.codeCountry != "" {
            self.codeCountryLabel.text = shareCountryIndex.codeCountry
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func goBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func contactsUsAction(_ sender: Any) {
        let destination = self.storyboard?.instantiateViewController(withIdentifier: "helpVC") as! HelpVC
        self.navigationController?.pushViewController(destination, animated: true)
    }
    
    @IBAction func cancelActionIssue(_ sender: Any) {
        self.issueView.isHidden = true
        self.backgroundView.isHidden = true
        
    }
    
    
    @IBAction func updatePhoneAction(_ sender: Any) {
        guard numberPhoneUser.text! != "" else {
            ShowAlert.sharedInstance.alertController(messageAlert: "Please fill phone field", title: "Error:", vc: self)
            return
        }
        //        let twilioSID = "twilioSID"
        //        let twilioSecret = "twilioSecret"
        //        //Note replace + = %2B , for To and From phone number
        //        let fromNumber = "%2BfromNumber"// actual number is +9999999
        //        //+18305417068
        //        let toNumber = "%2B373068514268"// actual number is +9999999
        //        let code = (Int(generateRandomDigits(6)))
        //        let message = String(code!)
        //
        //        // Build the request
        //        let request = NSMutableURLRequest(url: URL(string:"https://\(twilioSID):\(twilioSecret)@api.twilio.com/2010-04-01/Accounts/\(twilioSID)/SMS/Messages")!)
        //        request.httpMethod = "POST"
        //       // request.setValue("", forHTTPHeaderField: "")
        //        request.httpBody = "From=\(fromNumber)&To=\(toNumber)&Body=\(message)".data(using: .utf8)
        //
        //        // Build the completion block and send the request
        //        URLSession.shared.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) in
        //            print("Finished")
        //            if let data = data, let responseDetails = NSString(data: data, encoding: String.Encoding.utf8.rawValue) {
        //                // Success
        verifyCodeUser.unicCode = String((Int(generateRandomDigits(6)))!)
        verifyCodeUser.phoneNumberUser = self.codeCountryLabel.text! + " " + self.numberPhoneUser.text!
        let destination = self.storyboard?.instantiateViewController(withIdentifier: "signUpVerification1") as! SignUpVerificationVC1
        self.navigationController?.pushViewController(destination, animated: true)
        //                print("Response: \(responseDetails)")
        //            } else {
        //                // Failure
        //                print("Error: \(error)")
        //            }
        //        }).resume()
       
    }
    
    @IBAction func selectCountry(_ sender: Any) {
        shareCountryIndex.checkController = false
        let destination = self.storyboard?.instantiateViewController(withIdentifier: "countriesVC") as! CountriesVC
        self.navigationController?.pushViewController(destination, animated: true)
        
    }
    
    func generateRandomDigits(_ digitNumber: Int) -> String {
        var number = ""
        for i in 0..<digitNumber {
            var randomNumber = arc4random_uniform(10)
            while randomNumber == 0 && i == 0 {
                randomNumber = arc4random_uniform(10)
            }
            number += "\(randomNumber)"
        }
        return number
    }

    
   

}
