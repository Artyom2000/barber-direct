//
//  WelcomeScreenVC.swift
//  Barber Direct
//
//  Created by ABC on 11/29/17.
//  Copyright © 2017 ideactionio. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import FBSDKCoreKit


class WelcomeScreenVC: UIViewController, UINavigationControllerDelegate, UIViewControllerTransitioningDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor(patternImage: UIImage(named: "bd-img-2")!)
    self.view.transform = CGAffineTransform(scaleX: Constants.scale, y: Constants.scale)
        if UIApplication.shared.isRegisteredForRemoteNotifications{
            let token = UserDefaults.standard.object(forKey: "Token")
            print("token:", token ?? "no device token")
        }
 

        // Do any additional setup after loading the view.
    }



    @IBAction func goToSignIn(_ sender: Any) {
        let destination = self.storyboard?.instantiateViewController(withIdentifier: "loginVC") as! LoginVC
        self.navigationController?.pushViewController(destination, animated: true)
        
    }
    

    @IBAction func createAccountAction(_ sender: Any) {
        let destination = self.storyboard?.instantiateViewController(withIdentifier: "signUpVC") as! SignUpVC
        self.navigationController?.pushViewController(destination, animated: true)
        
    }
    
    
    
    @IBAction func facebookConnectBtn(_ sender: Any)
        {
            let facebookLogin = FBSDKLoginManager()
            facebookLogin.logIn(withReadPermissions: ["public_profile", "email"], from: self, handler:{(facebookResult, facebookError) -> Void in
                if facebookError != nil {
                    print("Facebook login failed. Error \(facebookError)")
                } else if (facebookResult?.isCancelled)! {
                    print("Facebook login was cancelled.")
                } else {

                    if FBSDKAccessToken.current() != nil {
                        
                        let userID = FBSDKAccessToken.current().userID
                        
                        if(userID != nil)
                        {
                            print(userID)
                        }
                        
                        // let profileImageUrl = NSURL(string: "http://graph.facebook.com/\(userID)/picture?type=large")!
                        
                        let url = URL(string: "http://barberdirect.project-demo.info/v1/sociallogin")!
                        
                        var concatenatedData:Data = Data();
                        let parametre:[String:Any] = ["Authtoken" : userID,
                                                      "Firstname": "testing",
                                                      "Email": "testing@gmail.com",
                                                      "Profileimage": String("http://graph.facebook.com/\(userID)/picture?type=large")]
                        let datas = try! JSONSerialization.data(withJSONObject: parametre, options: []);
                        concatenatedData.append(datas);
                        
                        let session = URLSession.shared
                        var request = URLRequest(url: url)
                        request.httpMethod = "POST"
                        request.httpBody = concatenatedData;
                        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                        request.addValue("application/json", forHTTPHeaderField: "Accept")
                        
                        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
                            
                            guard error == nil else {
                                return
                            }
                            
                            guard let data = data else {
                                return
                            }
                            do {
                                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] {
                                    print(json)
                                    DispatchQueue.main.async {
                                        if let dataUser  = json["data"] as? [String: Any] {
                                            UserInfo.firstName = (dataUser["firstName"] as? String)!
                                            UserInfo.lastName = (dataUser["lastName"] as? String)!
                                            UserInfo.userID = (dataUser["userId"] as? String)!
                                            UserInfo.email = (dataUser["email"] as? String)!
                                            UserInfo.profileImage = (dataUser["profileImage"] as? String)!
                                            UserInfo.phone = (dataUser["phone"] as? String)!
                                            UserInfo.createdOn = (dataUser["createdOn"] as? String)!
                                            let verificationCode = (dataUser["verificationCode"] as? String)!
                                            
                                            if verificationCode == ""
                                            {
                                                
                                                let destination = self.storyboard?.instantiateViewController(withIdentifier: "signUpVerificationVC") as! SignUpVerificationVC
                                                self.navigationController?.pushViewController(destination, animated: true)
                                            } else {
                                                let destination = self.storyboard?.instantiateViewController(withIdentifier: "accountTypeVC") as! AccountTypeVC
                                                self.navigationController?.pushViewController(destination, animated: true)
                                            }
                                        }
                                    }
                                    
                                }
                            } catch let error {
                                let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                                print(error.localizedDescription)
                            }
                        })
                        task.resume()
                        
                    }else{
                        let alert = UIAlertController(title: "Error", message: "Something goes wrong", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                        print("error with userID")

                    }
                    
                }
            });
    }
}
