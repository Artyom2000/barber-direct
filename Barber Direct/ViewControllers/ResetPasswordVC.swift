//
//  ResetPasswordVC.swift
//  Barber Direct
//
//  Created by SacaMarin on 11/12/2017.
//  Copyright © 2017 ideactionio. All rights reserved.
//

import UIKit

class ResetPasswordVC: UIViewController, UINavigationControllerDelegate {

    @IBOutlet weak var emailField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func resetPasswordAction(_ sender: Any) {
        guard self.emailField.text! != "" else {print("fill fields")
            return}
        let url = URL(string: "http://barberdirect.project-demo.info/v1/reset")!
        
        var concatenatedData:Data = Data();
        let parametre:[String:Any] = ["Username" : self.emailField.text!]
        let datas = try! JSONSerialization.data(withJSONObject: parametre, options: []);
        concatenatedData.append(datas);
        
        let session = URLSession.shared
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = concatenatedData;
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            
            guard error == nil else {
                return
            }
            
            guard let data = data else {
                return
            }
            
            do {
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] {
                    print(json)
                    print(json["message"] as? String)
//                    let destination = self.storyboard?.instantiateViewController(withIdentifier: "accountTypeVC") as! AccountTypeVC
//                    self.navigationController?.pushViewController(destination, animated: true)
                }
                
            } catch let error {
                print(error.localizedDescription)
            }
        })
        task.resume()
    }
    
    
    @IBAction func signUpAction(_ sender: Any) {
        let destination = self.storyboard?.instantiateViewController(withIdentifier: "signUpVC") as! SignUpVC
        navigationController?.pushViewController(destination, animated: true)
        self.present(destination, animated: true, completion: nil)
    }
    
}
