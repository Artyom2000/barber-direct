//
//  SignUpVC.swift
//  Barber Direct
//
//  Created by ABC on 11/29/17.
//  Copyright © 2017 ideactionio. All rights reserved.
//

import UIKit
import Foundation
import MobileCoreServices

class SignUpVC: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var lastNameField: UITextField!
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var firstNameField: UITextField!
    
    let path  = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString;
    
    let picker = UIImagePickerController()
    var localPath: URL?
    
    var checkImageSelected = false


    override func viewDidLoad() {
        super.viewDidLoad()
      
        customizeUI()
        self.view.transform = CGAffineTransform(scaleX: Constants.scale, y: Constants.scale)

        
        picker.delegate = self
       // self.imageView.layer.cornerRadius = 15
        self.imageView.clipsToBounds = true
        self.imageView.layer.cornerRadius = self.imageView.frame.size.width / 2;
       // self.profileImageView.clipsToBounds = YES;
        


        // Do any additional setup after loading the view.
    }
    //Hide
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }

    func customizeUI() {
        let layer = UIView(frame: CGRect(x: 30, y: 290, width: 315, height: 1))
        layer.alpha = 0.10
        layer.layer.borderWidth = 1
        layer.layer.borderColor = UIColor(red:0.11, green:0.11, blue:0.15, alpha:1).cgColor
        self.view.addSubview(layer)
        
        let layer1 = UIView(frame: CGRect(x: 30, y: 343, width: 315, height: 1))
        layer1.alpha = 0.10
        layer1.layer.borderWidth = 1
        layer1.layer.borderColor = UIColor(red:0.11, green:0.11, blue:0.15, alpha:1).cgColor
        self.view.addSubview(layer1)
        
        let layer2 = UIView(frame: CGRect(x: 30, y: 396, width: 315, height: 1))
        layer2.alpha = 0.10
        layer2.layer.borderWidth = 1
        layer2.layer.borderColor = UIColor(red:0.11, green:0.11, blue:0.15, alpha:1).cgColor
        self.view.addSubview(layer2)
        
        let layer3 = UIView(frame: CGRect(x: 30, y: 449, width: 315, height: 1))
        layer3.alpha = 0.10
        layer3.layer.borderWidth = 1
        layer3.layer.borderColor = UIColor(red:0.11, green:0.11, blue:0.15, alpha:1).cgColor
        self.view.addSubview(layer3)
        
        
        
        
    }
   
    @IBAction func closeThisVC(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func goBackToSignIn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    var token = UserDefaults.standard.object(forKey: "Token")

    @IBAction func createAccountAction(_ sender: Any) {
        guard self.emailField.text! != "", self.firstNameField.text! != "" , self.lastNameField.text! != "", self.passwordField.text! != "", self.checkImageSelected == true else {ShowAlert.sharedInstance.alertController(messageAlert: "Please, fill everthing (including image)", title: "Error", vc: self)
            return}
        AppDelegate.instance().showActivityIndicator()
        let request: URLRequest
        if token == nil{
            token = "nil"
        }
        do {
            request = try createRequest(firstname: self.firstNameField.text!, lastname: self.lastNameField.text!, email: self.emailField.text!, password: self.passwordField.text!, devicetype: "ios", devicetoken: token! as! String)
        } catch {
            print(error)
            AppDelegate.instance().dismissActivityIndicatos()

            let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            return
        }

        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard error == nil else {
                // handle error here
                print(error!)
                AppDelegate.instance().dismissActivityIndicatos()

                let alert = UIAlertController(title: "Error", message: error!.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                return
            }

            // if response was JSON, then parse it

            do {
                let json = try JSONSerialization.jsonObject(with: data!)
                print("success == \(json)")

                 DispatchQueue.main.async {
                    if (json as? [String: Any])?["status"] as? Int == 0 {
                        AppDelegate.instance().dismissActivityIndicatos()
                        let errorMessage = ((json as? [String: Any])?["message"] as? String)
                                            print(errorMessage!)
                                            let alert = UIAlertController(title: "Error", message: errorMessage!, preferredStyle: UIAlertControllerStyle.alert)
                                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                                            self.present(alert, animated: true, completion: nil)
                    } else {
                        print("2")
                    let data = (json as? [String: Any])?["data"] as? [String: Any]
                    UserInfo.firstName = (((json as? [String: Any])?["data"] as? [String: Any])?["firstName"]! as? String)!
                    UserInfo.lastName = (((json as? [String: Any])?["data"] as? [String: Any])?["lastName"]! as? String)!
                    UserInfo.userID = (((json as? [String: Any])?["data"] as? [String: Any])?["userId"]! as? String)!
                    UserInfo.email = (((json as? [String: Any])?["data"] as? [String: Any])?["email"]! as? String)!
                    UserInfo.profileImage = (((json as? [String: Any])?["data"] as? [String: Any])?["profileImage"]! as? String)!
                    UserInfo.createdOn = (((json as? [String: Any])?["data"] as? [String: Any])?["createdOn"]! as? String)!
                    let destination = self.storyboard?.instantiateViewController(withIdentifier: "signUpVerificationVC") as! SignUpVerificationVC
                    self.navigationController?.pushViewController(destination, animated: true)
                    
                }
            }
            } catch {
                print(error)
                AppDelegate.instance().dismissActivityIndicatos()

                let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                let responseString = String(data: data!, encoding: .utf8)
                print("responseString = \(responseString)")
            }
        }
        task.resume()
        
        
        
    }
    func createRequest(firstname: String, lastname: String, email: String, password: String, devicetype: String, devicetoken: String) throws -> URLRequest {
        let parameters = [
            "Firstname": firstname,
            "Lastname": lastname,
            "Email": email,
            "Password": password,
            "Devicetype": devicetype,
            "Devicetoken": devicetoken]
        
        let boundary = generateBoundaryString()
        
        let url = URL(string: "http://barberdirect.project-demo.info/v1/register")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        
       // let path1 = Bundle.main.path(forResource: "testingImage", ofType: "jpg")!
        let strdbpath =  self.path.strings(byAppendingPaths: ["profileImage.jpg"])[0]
        request.httpBody = try createBody(with: parameters, filePathKey: "Profileimage", paths: [strdbpath], boundary: boundary)
        
        return request
    }
    

    func createBody(with parameters: [String: String]?, filePathKey: String, paths: [String], boundary: String) throws -> Data {
        var body = Data()
        
        if parameters != nil {
            for (key, value) in parameters! {
                body.append("--\(boundary)\r\n")
                body.append("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
                body.append("\(value)\r\n")
            }
        }
        
        for path in paths {
            let url = URL(fileURLWithPath: path)
            let filename = url.lastPathComponent
            let data = try Data(contentsOf: url)
            let mimetype = mimeType(for: path)
            
            body.append("--\(boundary)\r\n")
            body.append("Content-Disposition: form-data; name=\"\(filePathKey)\"; filename=\"\(filename)\"\r\n")
            body.append("Content-Type: \(mimetype)\r\n\r\n")
            body.append(data)
            body.append("\r\n")
        }
        
        body.append("--\(boundary)--\r\n")
        return body
    }

    func generateBoundaryString() -> String {
        return "Boundary-\(NSUUID().uuidString)"
    }
    
    func mimeType(for path: String) -> String {
        let url = NSURL(fileURLWithPath: path)
        let pathExtension = url.pathExtension
        
        if let uti = UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, pathExtension! as NSString, nil)?.takeRetainedValue() {
            if let mimetype = UTTypeCopyPreferredTagWithClass(uti, kUTTagClassMIMEType)?.takeRetainedValue() {
                return mimetype as String
            }
        }
        return "application/octet-stream";
    }

    @IBAction func chooseCameraOption(_ sender: Any)
            {
        let alert = UIAlertController(title: "Profile picture", message: "Select from", preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addAction(UIAlertAction(title: "Camera", style: UIAlertActionStyle.default, handler: { action in
            
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
                let imagePicker = UIImagePickerController()
                imagePicker.delegate = self
                imagePicker.sourceType = UIImagePickerControllerSourceType.camera;
                imagePicker.allowsEditing = true
                self.present(imagePicker, animated: true, completion: nil)
            }
            
        }
        ))
        alert.addAction(UIAlertAction(title: "Gallery", style: UIAlertActionStyle.default, handler: {action in
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) {
                let imagePicker = UIImagePickerController()
                imagePicker.delegate = self
                imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary;
                imagePicker.allowsEditing = true
                
                self.present(imagePicker, animated: true, completion: nil)
            }
            
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil
        ))
        
        
        self.present(alert, animated: true, completion: nil)
        
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let image = info[UIImagePickerControllerEditedImage] as? UIImage {
            self.imageView.image = image
            let fileURL = self.documentsUrl.appendingPathComponent("profileImage" + ".jpg")
            if let imageData = UIImageJPEGRepresentation(image, 1.0) {
                try? imageData.write(to: fileURL, options: .atomic)
                print(self.path.strings(byAppendingPaths: ["profileImage.jpg"])[0])
                self.checkImageSelected = true
            } else  {
                print("Error saving image")
            }          
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    var documentsUrl: URL {
        return FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
    }
    
    
    
    
    
    
}
extension Data {

    mutating func append(_ string: String) {
        if let data = string.data(using: .utf8) {
            append(data)
        }
    }
}



