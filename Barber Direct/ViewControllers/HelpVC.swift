//
//  HelpVC.swift
//  Barber Direct
//
//  Created by ABC on 12/4/17.
//  Copyright © 2017 ideactionio. All rights reserved.
//

import UIKit

class HelpVC: UIViewController, UINavigationControllerDelegate {

    @IBOutlet weak var view0: UIView!
    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var phoneNumberField: UITextField!
    @IBOutlet weak var additionalDetailsField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let layer = UIView(frame: CGRect(x: 0, y: self.phoneNumberField.frame.origin.y + 30, width: 375, height: 1))
        layer.alpha = 0.05
        layer.layer.borderWidth = 1
        layer.layer.borderColor = UIColor(red:0.11, green:0.11, blue:0.15, alpha:1).cgColor
        self.view1.addSubview(layer)
        let layer1 = UIView(frame: CGRect(x: 0, y: self.additionalDetailsField.frame.origin.y + 30, width: 375, height: 1))
        layer1.alpha = 0.05
        layer1.layer.borderWidth = 1
        layer1.layer.borderColor = UIColor(red:0.11, green:0.11, blue:0.15, alpha:1).cgColor
        self.view1.addSubview(layer1)
        self.view.transform = CGAffineTransform(scaleX: Constants.scale, y: Constants.scale)

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func submitHelpAction(_ sender: Any) {
        guard phoneNumberField.text! != "", additionalDetailsField.text! != "" else {
            ShowAlert.sharedInstance.alertController(messageAlert: "Please fill all the fields", title: "Error:", vc: self)
            return
        }
        let url = URL(string: "http://barberdirect.project-demo.info/v1/Help")!
        
        var concatenatedData:Data = Data();
        let parametre:[String:Any] = ["Phonenumber" : "1243465",
                                      "Helpdetails": "testing",
                                      "Userid": "25"]
        let datas = try! JSONSerialization.data(withJSONObject: parametre, options: []);
        concatenatedData.append(datas);
        
        let session = URLSession.shared
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = concatenatedData;
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            
            guard error == nil else {
                return
            }
            
            guard let data = data else {
                return
            }
            
            do {
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] {
                    print(json)
                }
                
            } catch let error {
                print(error.localizedDescription)
            }
        })
        task.resume()
        
    }
    
    @IBAction func goBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    

}
