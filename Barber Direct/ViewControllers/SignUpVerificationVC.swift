//
//  SignUpVerificationVC.swift
//  Barber Direct
//
//  Created by ABC on 11/29/17.
//  Copyright © 2017 ideactionio. All rights reserved.
//

import UIKit
import Foundation

class SignUpVerificationVC: UIViewController, UINavigationControllerDelegate {


  
    @IBOutlet weak var phoneNumberUserField: UITextField!
    @IBOutlet weak var codeCountryLabel: UILabel!
    @IBOutlet weak var countryName: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        AppDelegate.instance().dismissActivityIndicatos()

        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
         view.addGestureRecognizer(tap)
        self.view.transform = CGAffineTransform(scaleX: Constants.scale, y: Constants.scale)
        if shareCountryIndex.nameCountry != "" {
            self.countryName.text = shareCountryIndex.nameCountry
        }
        if shareCountryIndex.codeCountry != "" {
            self.codeCountryLabel.text = shareCountryIndex.codeCountry
        }
        // Do any additional setup after loading the view.
    }
    func dismissKeyboard() {
        
        view.endEditing(true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func goToCountryList(_ sender: Any) {
        shareCountryIndex.checkController = true
        let destination = self.storyboard?.instantiateViewController(withIdentifier: "countriesVC") as! CountriesVC
        self.navigationController?.pushViewController(destination, animated: true)
    }
    
    
    
    
    
    func generateRandomDigits(_ digitNumber: Int) -> String {
        var number = ""
        for i in 0..<digitNumber {
            var randomNumber = arc4random_uniform(10)
            while randomNumber == 0 && i == 0 {
                randomNumber = arc4random_uniform(10)
            }
            number += "\(randomNumber)"
        }
        return number
    }
    @IBAction func nextButtonAction(_ sender: Any) {
        
//        let twilioSID = "twilioSID"
//        let twilioSecret = "twilioSecret"
//        let fromNumber = "%2B18305417068"// actual number is +9999999
//        //+18305417068
//        //let toNumber = "%2B373068514268"// actual number is +9999999
//        let codeNumber = self.codeCountryLabel.text?.replacingOccurrences(of: "+", with: "%2B")
//        let toNumber = codeNumber! + self.phoneNumberUserField.text!
//        let code = (Int(generateRandomDigits(6)))
//        let message = String(code!)
//
//        // Build the request
//        let request = NSMutableURLRequest(url: URL(string:"https://\(twilioSID):\(twilioSecret)@api.twilio.com/2010-04-01/Accounts/\(twilioSID)/SMS/Messages")!)
//        request.httpMethod = "POST"
//       // request.setValue("", forHTTPHeaderField: "")
//        request.httpBody = "From=\(fromNumber)&To=\(toNumber)&Body=\(message)".data(using: .utf8)
//
//        // Build the completion block and send the request
//        URLSession.shared.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) in
//            print("Finished")
//            if let data = data, let responseDetails = NSString(data: data, encoding: String.Encoding.utf8.rawValue) {
////                // Success
//                verifyCodeUser.unicCode = message
//                verifyCodeUser.phoneNumberResend = toNumber
//                DispatchQueue.main.sync {
//                    verifyCodeUser.phoneNumberUser = self.codeCountryLabel.text! + self.phoneNumberUserField.text!
//                    print(verifyCodeUser.phoneNumberUser)
//                    let destination = self.storyboard?.instantiateViewController(withIdentifier: "signUpVerification1") as! SignUpVerificationVC1
//                    self.navigationController?.pushViewController(destination, animated: true)
//
//                }
//
//                print("Response: \(responseDetails)")
//            } else {
//                // Failure
//                print("Error: \(error)")
//            }
//        }).resume()
        let destination = self.storyboard?.instantiateViewController(withIdentifier: "signUpVerification1") as! SignUpVerificationVC1
            self.navigationController?.pushViewController(destination, animated: true)

    }

  
}
struct verifyCodeUser {
   static var unicCode = ""
    static var phoneNumberUser = ""
    static var phoneNumberResend = ""
    
    
}
