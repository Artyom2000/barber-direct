//
//  FirstScreenVC.swift
//  Barber Direct
//
//  Created by Artyom Schiopu on 12/29/17.
//  Copyright © 2017 ideactionio. All rights reserved.
//

import UIKit

class FirstScreenVC: UIViewController {


    @IBAction func barberServiceAction(_ sender: UIButton) {
        let destination = self.storyboard?.instantiateViewController(withIdentifier: "welcomeScreenVC") as! WelcomeScreenVC
        self.navigationController?.pushViewController(destination, animated: true)
    }
    @IBAction func powerUpBusinessAction(_ sender: UIButton) {
        let destination = self.storyboard?.instantiateViewController(withIdentifier: "WelcomeScreenForBarbersVC") as! WelcomeScreenForBarbersVC
        self.navigationController?.pushViewController(destination, animated: true)
    }
    override func viewDidLoad() {
        super.viewDidLoad()


    }

}
