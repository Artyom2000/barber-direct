//
//  PostJobVC.swift
//  Barber Direct
//
//  Created by Artyom Schiopu on 12/8/17.
//  Copyright © 2017 ideactionio. All rights reserved.
//

import UIKit

class PostJobVC: UIViewController {

   
    @IBOutlet weak var topView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func postJobAction(_ sender: UIButton) {
        let alert = UIAlertController(title: "Job successfuly posted", message: nil, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: {(alert: UIAlertAction!) in
            self.topView.backgroundColor = UIColor(red:0.31, green:0.82, blue:0.76, alpha:1)
            self.topView.alpha = 1
        }))
        
        self.present(alert, animated: true, completion: nil )
      
       topView.backgroundColor = UIColor.black
        topView.alpha = 0.4
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
