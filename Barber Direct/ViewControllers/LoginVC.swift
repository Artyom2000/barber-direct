//
//  LoginVC.swift
//  Barber Direct
//
//  Created by ABC on 11/29/17.
//  Copyright © 2017 ideactionio. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit




class LoginVC: UIViewController, UINavigationControllerDelegate {

    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var emailField: UITextField!

    
    override func viewDidLoad() {
        super.viewDidLoad()

        customizeUI()
        self.view.transform = CGAffineTransform(scaleX: Constants.scale, y: Constants.scale)

        // Do any additional setup after loading the view.
    }
    //Hide
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }

    
    func customizeUI() {
        let layer = UIView(frame: CGRect(x: 30, y: 371.5, width: 315, height: 1))
        layer.alpha = 0.10
        layer.layer.borderWidth = 1
        layer.layer.borderColor = UIColor(red:0.11, green:0.11, blue:0.15, alpha:1).cgColor
        self.view.addSubview(layer)
        let layer0 = UIView(frame: CGRect(x: 30, y: 298.5, width: 315, height: 1))
        layer0.alpha = 0.10
        layer0.layer.borderWidth = 1
        layer0.layer.borderColor = UIColor(red:0.11, green:0.11, blue:0.15, alpha:1).cgColor
        self.view.addSubview(layer0)
    }
    
    @IBAction func goToSignUp(_ sender: Any) {
     
        let destination = self.storyboard?.instantiateViewController(withIdentifier: "signUpVC") as! SignUpVC
        self.navigationController?.pushViewController(destination, animated: true)
    }
    
    @IBAction func forgotPasswordAction(_ sender: Any) {
      
        let destination = self.storyboard?.instantiateViewController(withIdentifier: "resetPasswordVC") as! ResetPasswordVC
        self.navigationController?.pushViewController(destination, animated: true)
    }

    @IBAction func signInFacebookAction(_ sender: Any) {
        let facebookLogin = FBSDKLoginManager()
        facebookLogin.logIn(withReadPermissions: ["public_profile", "email"], from: self, handler:{(facebookResult, facebookError) -> Void in
            if facebookError != nil {
                print("Facebook login failed. Error \(facebookError)")
            } else if (facebookResult?.isCancelled)! {
                print("Facebook login was cancelled.")
            } else {

                if FBSDKAccessToken.current() != nil {
                    
                    let userID = FBSDKAccessToken.current().userID
                    
                    if(userID != nil)
                    {
                        print(userID)
                    }
                    
                    // let profileImageUrl = NSURL(string: "http://graph.facebook.com/\(userID)/picture?type=large")!
                    
                    let url = URL(string: "http://barberdirect.project-demo.info/v1/sociallogin")!
                    
                    var concatenatedData:Data = Data();
                    let parametre:[String:Any] = ["Authtoken" : userID,
                                                  "Firstname": "testing",
                                                  "Email": "testing@gmail.com",
                                                  "Profileimage": String("http://graph.facebook.com/\(userID)/picture?type=large")]
                    let datas = try! JSONSerialization.data(withJSONObject: parametre, options: []);
                    concatenatedData.append(datas);
                    
                    let session = URLSession.shared
                    var request = URLRequest(url: url)
                    request.httpMethod = "POST"
                    request.httpBody = concatenatedData;
                    request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                    request.addValue("application/json", forHTTPHeaderField: "Accept")
                    
                    let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
                        
                        guard error == nil else {
                            return
                        }
                        
                        guard let data = data else {
                            return
                        }
                            do {
                            if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] {
                                print(json)
                                DispatchQueue.main.async {
                                    if let dataUser  = json["data"] as? [String: Any] {
                                        UserInfo.firstName = (dataUser["firstName"] as? String)!
                                        UserInfo.lastName = (dataUser["lastName"] as? String)!
                                        UserInfo.userID = (dataUser["userId"] as? String)!
                                        UserInfo.email = (dataUser["email"] as? String)!
                                        UserInfo.profileImage = (dataUser["profileImage"] as? String)!
                                        UserInfo.phone = (dataUser["phone"] as? String)!
                                        UserInfo.createdOn = (dataUser["createdOn"] as? String)!
                                        let verificationCode = (dataUser["verificationCode"] as? String)!

                                        if verificationCode == ""
                                        {
                                            
                                            let destination = self.storyboard?.instantiateViewController(withIdentifier: "signUpVerificationVC") as! SignUpVerificationVC
                                            self.navigationController?.pushViewController(destination, animated: true)
                                        } else {
                                            let destination = self.storyboard?.instantiateViewController(withIdentifier: "accountTypeVC") as! AccountTypeVC
                                            self.navigationController?.pushViewController(destination, animated: true)
                                        }
                                    }
                                }
                                
                            }
                        } catch let error {
                            let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                            print(error.localizedDescription)
                        }
                    })
                    task.resume()
           
                }else{
                    let alert = UIAlertController(title: "Error", message: "Something goes wrong", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    print("error with userID")
                }
               

            }
        });
    }
    
    
    
    
    @IBAction func signInActionCustom(_ sender: Any) {
        guard emailField.text! != "", passwordField.text! != "" else {
            ShowAlert.sharedInstance.alertController(messageAlert: "Please fill all the fields", title: "Error:", vc: self)
            return
        }
        let url = URL(string: "http://barberdirect.project-demo.info/v1/login")!
        
        var concatenatedData:Data = Data();
        let parametre:[String:Any] = ["Username" : self.emailField.text!,
                                      "Password": self.passwordField.text!,
                                      "Devicetype": "ios",
                                      "Devicetoken": "asdasd1e"]
       
        let datas = try! JSONSerialization.data(withJSONObject: parametre, options: []);
        concatenatedData.append(datas);
        
        let session = URLSession.shared
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = concatenatedData;
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            
            guard error == nil else {
                print(error?.localizedDescription)
                return
            }
            
            guard let data = data else {
                
                return
            }
            do {
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] {
                    DispatchQueue.main.async {
                    if let dataUser  = json["data"] as? [String: Any] {
                        UserInfo.firstName = (dataUser["firstName"] as? String)!
                        UserInfo.lastName = (dataUser["lastName"] as? String)!
                        UserInfo.userID = (dataUser["userId"] as? String)!
                        UserInfo.email = (dataUser["email"] as? String)!
                        UserInfo.profileImage = (dataUser["profileImage"] as? String)!
                        UserInfo.phone = (dataUser["phone"] as? String)!
                        UserInfo.createdOn = (dataUser["createdOn"] as? String)!
                        let verificationCode = (dataUser["verificationCode"] as? String)!
                        print(dataUser)
                        if verificationCode == ""
                        {
                        
                                    let destination = self.storyboard?.instantiateViewController(withIdentifier: "signUpVerificationVC") as! SignUpVerificationVC
                                    self.navigationController?.pushViewController(destination, animated: true)
                            } else {
                                              let destination = self.storyboard?.instantiateViewController(withIdentifier: "accountTypeVC") as! AccountTypeVC
                                               self.navigationController?.pushViewController(destination, animated: true)
                            }
                        }
                    }
                    
                        
                    
                    
                    
                }
            } catch let error {
                print(error.localizedDescription)
                let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        })
        task.resume()
        
    }
  


}
struct UserInfo {
    static var createdOn = ""
    static var email = ""
    static var firstName = ""
    static var lastName = ""
    static var phone = ""
    static var profileImage = ""
    static var userID = ""
    
}
