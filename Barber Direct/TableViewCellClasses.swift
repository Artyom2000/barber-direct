//
//  TableViewCellClasses.swift
//  Barber Direct
//
//  Created by ABC on 12/1/17.
//  Copyright © 2017 ideactionio. All rights reserved.
//

import Foundation
import UIKit


class CountriesTBCell: UITableViewCell {
    
    @IBOutlet weak var checkSelectedCountry: UIImageView!
    @IBOutlet weak var checkLineCountry: UIImageView!
    @IBOutlet weak var countryName: UILabel!
    @IBOutlet weak var codeCountry: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
     
    }
    
}
class AccountTypeCell: UITableViewCell {
    
    @IBOutlet var profileImage: UIImageView!
    @IBOutlet var profileName: UILabel!
    @IBOutlet var typeOfAccount: UILabel!
    @IBOutlet var checkMarkView: UIImageView!

    @IBOutlet var doneImage: UIImageView!

    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    

}
