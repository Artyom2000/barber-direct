//
//  ShowAlert.swift
//  Barber Direct
//
//  Created by Artyom Schiopu on 12/13/17.
//  Copyright © 2017 ideactionio. All rights reserved.
//

import UIKit

class ShowAlert: NSObject {
    static let sharedInstance = ShowAlert()
    
    func alertController(messageAlert: String, title: String, vc: UIViewController){
        let alert = UIAlertController(title: "Error", message: messageAlert, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        vc.present(alert, animated: true, completion: nil)
        
    }
}
